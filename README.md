

# Adding items to the database 

There are two places you need to update:

## 1. Right click ShoppingCartSimulation --> Properties --> Resources

Arguments in line 1 are the grocery item categories, separated by a comma.  *You should not remove any,* but if you wish to add another category, such as detergents, paper products, etc, feel free.

Lines 2+ are the items, with arguments separated by a comma.

  Arguments in Line 2+:
  
	1. Item ID, three digit number.  
	  First digit is the category, digits 
	  2-3 are the item ID.  (starting with 01)  
	  So item 201 is the first item in category 2.
	
	2. Item name, as shown in the receipt.  
	  Keep it up to 10 characters so the receipt
	  doesn't look weird.
	
	3. The price of the item
	
	4. Whether or not it's a tax.  Type in a
	  "1" if it's taxed, "0" if not.
## 2. Using the VS solution explorer, add a bitmap image into folder /Resources2/
Set the name of the item to "###.jpg", where ### is the item ID.

## 3. App.xaml: in <Application.Resources>, add a bitmap image with the key of the Item ID.
See existing items in Application.Resources


# Using the Shopping Center 
 1. On the left is the list of items you have purchased.
 2. To select items, look at the images on the right.
 3. If you hover over them, you can see the details of the item,  including the item ID.  If you type the item ID into the top right text box, you can include that one item.
 4. If you added an item accidentally, click the "Void Item" button to take out the last thing you put in.
 5. If you want to start over, click "Clear Cart."
 6. When you're ready, click "Check Out"


# Checking out

 1. If you prefer to pay with credit card, press the Credit Card radio button.  Then fill out your credit card information.   The expiration date is shown on your card, showing the month and year.  For instance, is January 2021 is the expiration date, do 01/21.  The security code is located on the back of your card and is three digits.  If you want cash back, type any amount.
 2. If you prefer to pay cash, keep the radio button the over cash.  Be sure to type in an amount greater than the total or you cannot pay.
 3. Once the pay button is enabled, click the pay button and your receipt will open in the browser.
