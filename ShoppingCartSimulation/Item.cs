﻿using System.Windows.Media.Imaging;

/// <summary>
/// Generic entry into the database
/// </summary>
namespace ShoppingCartSimulation.Data
{
    /// <summary>
    /// Generic Item to putin the Item DB.  
    /// Should be paired with an integer (item code)
    /// </summary>
    public class Item
    {
        string name;
        double price;
        BitmapImage image;
        bool tax;
        const double TAX_RATE = 0.07;

        /// <summary>
        /// Create an item.
        /// </summary>
        /// <param name="n">Item name, as shown on rececipt</param>
        /// <param name="id">Item ID in the database to load the Image to display on the database.</param>
        /// <param name="p">The price of the item before tax</param>
        /// <param name="t">Whether or not tax is applied (see: TAX_RATE)</param>
        public Item(string n, int id, double p, bool t)
        {
            //image = new BitmapImage();
           
            image = App.Current.FindResource(id.ToString()) as BitmapImage;
            name = n;
            price = p;
            tax = t;
        }

        /// <summary>
        /// Get the price before tax.  Full price will be calculated at checkout.
        /// </summary>
        public double SubPrice
        {
            get { return price; }
        }

        /// <summary>
        /// Get the tax (price * TAX_RATE)
        /// </summary>
        public double Tax
        {
            get {
                if (tax == true)
                    return price * TAX_RATE;
                else
                    return 0.0;
            }
        }

        /// <summary>
        /// Get the name of the item, as shown on the receipt
        /// </summary>
        public string Name
        {
            get { return name; }
        }

        /// <summary>
        /// Get the image representing the item.
        /// </summary>
        public BitmapImage Image
        {
            get { return image; }
        }

    }
}
