﻿using System;
using System.IO;
using System.Collections.Generic;

namespace ShoppingCartSimulation.Data
{
    /// <summary>
    /// Handler data / code for the items you have purchased.
    /// </summary>
    public class ShoppingCart
    {
        protected List<int> cart;

        /// <summary>
        /// Get the total before taxes.
        /// </summary>
        public double SubTotal
        {
            get
            {
                double subTotal = 0;
                List<int>.Enumerator e1 = cart.GetEnumerator();
                while(e1.MoveNext())
                {
                    subTotal += ItemDB.Instance.GetItem(e1.Current).SubPrice;
                }
                return subTotal;
            }
        }

        /// <summary>
        /// Get the total tax
        /// </summary>
        public double Tax
        {
            get
            {
                double subTotal = 0;
                List<int>.Enumerator e1 = cart.GetEnumerator();
                while (e1.MoveNext())
                {
                    subTotal += ItemDB.Instance.GetItem(e1.Current).Tax;
                }
                return subTotal;
            }
        }

        /// <summary>
        /// Get the total after taxes in your shopping cart.
        /// </summary>
        public double Total
        {
            get
            {
                double subTotal = 0;
                List<int>.Enumerator e1 = cart.GetEnumerator();
                while (e1.MoveNext())
                {
                    Item it = ItemDB.Instance.GetItem(e1.Current);
                    subTotal += it.Tax;
                    subTotal += it.SubPrice;
                }
                return subTotal;
            }
        }

        /// <summary>
        /// Void the last item in the mini receipt
        /// </summary>
        public void Undo()
        {
            cart.RemoveAt(cart.Count-1);
            PrintReceipt();
        }

        public ShoppingCart()
        {
            ItemDB.Instance.ToString();  //Init the database immediately
            cart = new List<int>();
        }

        /// <summary>
        /// Get a small summary of the items (item name & price before tax)
        /// </summary>
        /// <returns>An array of ItemDesc structs</returns>
        private ItemDesc[] GetItemDescs()
        {
            ItemDesc[] list = new ItemDesc[cart.Count];
            int i = 0;

            List<int>.Enumerator e1 = cart.GetEnumerator();
            while(e1.MoveNext())
            {
                Item it = ItemDB.Instance.GetItem(e1.Current);
                list[i].Name = it.Name;
                list[i].Price = it.SubPrice;
                i++;
            }

            return list;
        }

        /// <summary>
        /// Add an item to the cart and print to the main window
        /// </summary>
        /// <param name="id">Item ID</param>
        public void AddItemToCart(int id)
        {
            Console.WriteLine("Adding ID " + id + " to cart.");
            cart.Add(id);
            PrintReceipt();
        }

        /// <summary>
        /// Print to the Main Window UI 
        /// </summary>
        public void PrintReceipt()
        {
            string s = "";
            ItemDesc[] ds = GetItemDescs();
            for(int i = 0; i < ds.Length; i++)
            {
                if (i == 0)  //First line- no new line character
                    s = s + ds[i].Name + ": $" + ds[i].Price.ToString("0.00");
                else //2+ lines- start with new line character.
                    s = s + "\n" + ds[i].Name + ": $" + ds[i].Price.ToString("0.00");

            }

            string s2 = "\n\nSubtotal: $" + SubTotal.ToString("0.00") +
                "\nTaxes: $" + Tax.ToString("0.00") +
                "\nTotal: $" + Total.ToString("0.00");


            MainWindow.Instance.UpdateText(s, s2);
        }

        /// <summary>
        /// Create an HTML receipt and reset the shopping cart.
        /// </summary>
        /// <param name="isCash">Do you pay with cash?</param>
        /// <param name="paidAmount">Amount you paid</param>
        /// <param name="customerName">Name of the customer on the order form</param>
        private void ExportToHTML(bool isCash, double paidAmount, string customerName)
        {

            string fileContent = Properties.Resources.ReceiptTemplete;
            string s = "";
            ItemDesc[] list = GetItemDescs();
            for(int i = 0; i < list.Length; i++)
            {
                s = s + "<tr><td>" + list[i].Name + "</td><td>$" + list[i].Price.ToString("0.00") + "</td></tr>";
            }

            fileContent = fileContent.Replace("[Table]", s);

            DateTime current = DateTime.Now;
            fileContent = fileContent.Replace("[Date]", current.ToShortDateString());
            fileContent = fileContent.Replace("[Time]", current.ToShortTimeString());

            double total = Total;
            string s2 = "Sub Total: $" + SubTotal.ToString("0.00") + "<br />"
                + "Sales tax: $" + Tax.ToString("0.00") + "<br />"
                + "Grand Total: $" + total.ToString("0.00") + "<br />";

            double change = paidAmount - total;
            if(isCash)
            {
                s2 = s2 + "Cash Payment: $" + paidAmount.ToString("0.00")
                    + "<br />Change: $" + change.ToString("0.00");
            }
            else
            {
                s2 = s2 + "Credit Payment: $" + paidAmount.ToString("0.00")
                    + "<br />Cash Back: $" + change.ToString("0.00"); 
            }

            fileContent = fileContent.Replace("[Total]", s2);

            fileContent = fileContent.Replace("[Cashier]", customerName);

            string fileName = current.Month + " " + current.Day + " " + current.Millisecond + ".html";
            File.WriteAllText(fileName, fileContent);

            System.Diagnostics.Process.Start(fileName);
        }

        /// <summary>
        /// Clear everything in the cart and refresh the main window UI.
        /// </summary>
        public void ResetShoppingCart()
        {
            cart.Clear();
            PrintReceipt();
        }

        /// <summary>
        /// Pay for the order, export the receipt to HTML, and reset the shopping cart.
        /// </summary>
        /// <param name="isCash">True if you're paying with cash</param>
        /// <param name="paidAmount">Amount including change</param>
        /// <param name="customerName">Name of the customer on the order form</param>
        public void Pay(bool isCash, double paidAmount, string customerName)
        {
            ExportToHTML(isCash, paidAmount, customerName);
            ResetShoppingCart();

        }
    }

    
}
