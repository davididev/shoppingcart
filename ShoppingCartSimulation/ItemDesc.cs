﻿

namespace ShoppingCartSimulation.Data
{
    /// <summary>
    /// A simple struct representing items on the receipt
    /// </summary>
    public struct ItemDesc
    {
        public string Name;
        public double Price;
    }
}
