﻿using System;
using System.Collections.Generic;

namespace ShoppingCartSimulation.Data
{
    /// <summary>
    /// Database for all the items.  
    /// This class sohuld be treated as read only.
    /// </summary>
    public class ItemDB
    {
        private SortedDictionary<int, Item> items;
        private static ItemDB instance;

        //public static string[] CATEGORIES = { "All", "Drinks", "Electronics", "Food" };
        public static string[] CATEGORIES;

        /// <summary>
        /// Get the instance, or create it if it doesn't exist.
        /// </summary>
        public static ItemDB Instance
        {
            get
            {
                if (instance == null)
                    instance = new ItemDB();

                return instance;
            }
        }

        /// <summary>
        /// Set the variable "DB" to get local variable "items".
        /// This is to prevent setting it from outside the class.
        /// </summary>
        public SortedDictionary<int, Item> DB => items;

        /// <summary>
        /// Get a "list" by item code for the lookup item function.
        /// </summary>
        /// <returns>Either one item or an empty list (empty list if code doesn't exist)</returns>
        public SortedDictionary<int, Item> GetListByCode(int code)
        {
            SortedDictionary<int, Item> ret = new SortedDictionary<int, Item>();

            Item i;
            if(items.TryGetValue(code, out i))
                ret.Add(code, i);

            return ret;
        }

        /// <summary>
        /// Create the database.  If you add more items, this is where you go.
        /// </summary>
        private void InitDatabase()
        {
            items = new SortedDictionary<int, Item>();
            string[] lines = Properties.Resources.Database.Replace("\r\n", "\n").Split('\n');


            //Set the categories (first line in Database)
            string[] categories = lines[0].Split(',');
            CATEGORIES = new string[categories.Length];
            for(int i = 0; i < categories.Length; i++)
            {
                CATEGORIES[i] = categories[i];
            }

            //Set the items
            for(int i = 1; i < lines.Length; i++)
            {
                string[] args = lines[i].Split(',');
                try
                {
                    int id = int.Parse(args[0]);
                    string name = args[1];
                    double price = double.Parse(args[2]);
                    bool isTaxed = false;
                    if (args[3] == "1")
                        isTaxed = true;

                    items.Add(id, new Item(name, id, price, isTaxed));
                }
                catch(Exception e)
                {
                    Console.WriteLine("Error reading from the database.  " + e.ToString());
                }
            }

            /*
             * Old code.  We're going to get it from Database.txt now.
            //Let's do the drinks.
            items.Add(101, new Item("12oz Water", 101, 1.00, true));
            items.Add(102, new Item("16oz ApplJ", 102, 2.89, false));
            items.Add(103, new Item("16oz Soda", 103, 1.99, true));

            //Add the electronics
            items.Add(201, new Item("8ct AAABat", 201, 6.99, true));

            //Add the food
            items.Add(301, new Item("Sml Pretzl", 301, 0.99, false));
            */
        }

        public ItemDB()
        {
            InitDatabase();
        }

        /// <summary>
        /// Get all the items in Category (X)
        /// </summary>
        /// <param name="category">Category ID.  If it's 0, get the whole list.</param>
        /// <returns></returns>
        public SortedDictionary<int, Item> GetListInCategory(int category)
        {
            SortedDictionary<int, Item> l = new SortedDictionary<int, Item>();
            if(category > 0)
            {
                int min = (category * 100) + 1;
                int max = min + 99;
                for (int i = min; i < max; i++)
                {
                    Item it;
                    if (items.TryGetValue(i, out it))
                        l.Add(i, it);
                    else
                        break;  //End the loop if there's nothing left to show.
                
                }
            }
            if(category == 0)
            {
                SortedDictionary<int, Item>.Enumerator e1 = items.GetEnumerator();
                while(e1.MoveNext())
                {
                    l.Add(e1.Current.Key, e1.Current.Value);
                }
            }
            return l;
        }

        public Item GetItem(int id)
        {
            Item it;
            if (items.TryGetValue(id, out it) == true)
            {
                return it;
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }
    }
}
