﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using ShoppingCartSimulation.Data;

namespace ShoppingCartSimulation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MediaPlayer mediaPlayer;
        private int itemCode;
        public static MainWindow Instance;
        public ShoppingCart cart { get; private set; }
        


        public MainWindow()
        {
            InitializeComponent();
            mediaPlayer = new MediaPlayer();
            itemCode = -1;
            cart = new ShoppingCart();
            Instance = this;


            cart.PrintReceipt();
            UpdateCheckoutButton();
            SetCategories();
        }

        /// <summary>
        /// Init the combo box (Draw the categories in the combo box, 
        /// set it to default, and draw the items.)
        /// </summary>
        void SetCategories()
        {
            for(int i = 0; i < ItemDB.CATEGORIES.Length; i++)
            {
                ItemCategoryCombo.Items.Add(ItemDB.CATEGORIES[i]);
            }

            ItemCategoryCombo.SelectedIndex = 0;
            DisplayItems();

        }

        /// <summary>
        /// Enable or Disable the pay button
        /// </summary>
        void UpdateCheckoutButton()
        {
            if (cart.Total == 0)
                PayButton.IsEnabled = false;
            else
                PayButton.IsEnabled = true;
        }

        /// <summary>
        /// Update the Shopping Cart Mini Receipt
        /// </summary>
        /// <param name="receiptText">individual items to add</param>
        /// <param name="totalText">Subtotal, tax, and total</param>
        public void UpdateText(string receiptText, string totalText)
        {
            if (receiptText == "")
                MiniReceipt.Text = "No items added yet.";
            else
                MiniReceipt.Text = receiptText;
            MiniReceipt.Height = 15 * receiptText.Split('\n').Length;

            Total.Text = totalText;
        }

        /// <summary>
        /// Display the images of the items you wish to add.
        /// Should be called only on Init and when the Combo Box is changed, 
        /// and when a price entry code is written in the text box
        /// </summary>
        void DisplayItems()
        {
            SortedDictionary<int, Item> list = new SortedDictionary<int, Item>();
            if (itemCode == -1)  //Showing a list
            {
                Console.WriteLine("Selected index: " + ItemCategoryCombo.SelectedIndex);
                list = ItemDB.Instance.GetListInCategory(ItemCategoryCombo.SelectedIndex);

            }
            else  //Selected one item
            {
                list = ItemDB.Instance.GetListByCode(itemCode);
            }

            ItemsStackPanel.Children.Clear();
            ItemsStackPanel.Height = 128 * list.Count;
            SortedDictionary<int, Item>.Enumerator e1 = list.GetEnumerator();
            while (e1.MoveNext())
            {
                Button newBtn = new Button();

                newBtn.Height = 128;
                newBtn.Width = 128;
                Item it = e1.Current.Value;
                newBtn.Name = "ButtonItem" + e1.Current.Key;
                newBtn.ToolTip = it.Name + "(Ref Code: " + e1.Current.Key + ")\n$" + it.SubPrice.ToString("0.00") +
                    (it.Tax == 0f ? "\nNot Taxed" : "\nTaxed ($" + it.Tax.ToString("0.00") + ")");
                newBtn.Click += AddItem;
                //ImageBrush img = new Uri("Images/101.jpg", UriKind.Relative);
                Rectangle rect = new Rectangle();
                rect.Height = 128;
                rect.Width = 128;
                rect.Fill = new ImageBrush(it.Image);

                newBtn.Content = rect;

                ItemsStackPanel.Children.Add(newBtn);
            }

        }

        /// <summary>
        /// Add An Item to the shopping list (ITem button)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AddItem(object sender, RoutedEventArgs e)
        {
            Button b = (Button)e.Source;
            string idStr = b.Name.Substring(10);
            int id = int.Parse(idStr);
            cart.AddItemToCart(id);

            //Play sound effect
            System.IO.Stream stream = Properties.Resources.BeepFX;
            System.Media.SoundPlayer snd = new System.Media.SoundPlayer(stream);
            snd.Play();

            Console.WriteLine("Pressed button " + b.Name);
            UpdateCheckoutButton();
        }

        
        /// <summary>
        /// When the combo box of item image cateogries is selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SelectCategory(object sender, EventArgs e)
        {
            LookupCode.Text = "";
            itemCode = -1;
            DisplayItems();
        }

        /// <summary>
        /// When the clear cart button is pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ClearCart(object sender, EventArgs e)
        {
            cart.ResetShoppingCart();
            UpdateCheckoutButton();
        }

        /// <summary>
        /// When the pay button is pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void PayForItems(object sender, EventArgs e)
        {
            if(cart.Total > 0)
            {
                PayWindow newWindow = new PayWindow(cart, CustomerName.Text);
                newWindow.ShowDialog();

                PayButton.IsEnabled = false;
            }
            
        }

        private void LookupCode_TextChanged(object sender, TextChangedEventArgs e)
        {
            string str = LookupCode.Text;
            

            int code = 0;
            if(int.TryParse(str, out code))
            {
                itemCode = code;
            }
            else
            {
                str = "";
                itemCode = -1;
            }

            //ItemCategoryCombo.IsEnabled = (str == "");
            DisplayItems();
        }

        private void UndoButton_Click(object sender, RoutedEventArgs e)
        {
            cart.Undo();
        }
    }
}
