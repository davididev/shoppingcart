﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace ShoppingCartSimulation
{
    /// <summary>
    /// Interaction logic for CreditCardPage.xaml
    /// </summary>
    public partial class CreditCardPage : Page
    {
        PayWindow parent;
        double total;
        double payAmount;

        bool negativeError, invalidCreditCardError, invalidExpirationError, invalidCodeError;

        public CreditCardPage(double t, PayWindow p)
        {
            InitializeComponent();
            total = t;
            payAmount = total;
            parent = p;

            negativeError = false;
            invalidCreditCardError = false;
            invalidCodeError = false;
            invalidExpirationError = false;

            parent.PayButton.IsEnabled = false;
            UpdateErrorText();
        }

        /// <summary>
        /// Print all possible errors.
        /// </summary>
        void UpdateErrorText()
        {
            if (parent == null)  //If this function is called before the constructor
                return;
            string s = "";
            if (negativeError)
                s = s + "[Negative amount] ";
            if (invalidCreditCardError)
                s = s + "[Invalid credit card] ";
            if (invalidExpirationError)
                s = s + "[Invalid expiration] ";
            if (invalidCodeError)
                s = s + "[Invalid confirmation code] ";
            parent.ErrorText.Text = s;

            parent.PayButton.IsEnabled = (s == "");  //Enable pay button if no errors.

        }

        /// <summary>
        /// When the cash back text box is changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ChangeCashBack(object sender, EventArgs e)
        {
            if (CashBackText == null)
                return;
            double amt = 0.0;
            if (double.TryParse(CashBackText.Text, out amt))
            {
                //Si
                negativeError = amt < 0;  //If less than zero, set error to true
                UpdateErrorText();
            }
            else
            {
                CashBackText.Text = "0.00";
            }
        }

        /// <summary>
        /// When the credit card number text box is changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ChangeCreditCardNumber(object sender, EventArgs e)
        {
            string reg = "\\d{4}-\\d{4}-\\d{4}-\\d{4}";  //####-####-####-####
            invalidCreditCardError = Regex.IsMatch(CreditCardNumber.Text, reg) == false;
            UpdateErrorText();
        }

        /// <summary>
        /// When the expiration date text box is changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ChangeExpirationDate(object sender, EventArgs e)
        {
            string reg = "[0-9][0-9]/[0-9][0-9]";  //Month (01 to 12) and year (10-99)
            if (Regex.IsMatch(Expiration.Text, reg) == true)  //It is a match, but let's check the date.
            {
                try
                {
                    string[] paramsStr = Expiration.Text.Split('/');
                    int month = int.Parse(paramsStr[0]);
                    if(month > 0  && month <= 12)
                    {
                        int year = int.Parse(paramsStr[1]) + 2000;

                        DateTime now = DateTime.Now;
                        if (year < now.Year)  //Year put in is less than current year
                        {
                            invalidExpirationError = true;
                        }
                        else if (year == now.Year)  //Year put in is current year 
                        {
                            if (month < now.Month)
                                invalidExpirationError = true;
                            else
                                invalidExpirationError = false;
                        }
                        else  //Year put in is greater
                        {
                            invalidExpirationError = false;
                        }
                    }
                    else  //Month is valid
                    {
                        invalidExpirationError = true;
                    }


                }
                catch(Exception e1)
                {
                    Console.WriteLine(e1.ToString());
                    invalidExpirationError = true;
                }
            }
            else
                invalidExpirationError = true;
            UpdateErrorText();
        }

        /// <summary>
        /// When the Confirmation Code text box is changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ChangeConfirmationCode(object sender, EventArgs e)
        {
            string reg = "[1-9][1-9][1-9]";  //three digit number
            invalidCodeError = Regex.IsMatch(ConfirmationCode.Text, reg) == false;
            UpdateErrorText();
        }
    }
}
