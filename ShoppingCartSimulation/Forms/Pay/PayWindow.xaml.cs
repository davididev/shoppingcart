﻿using System;
using System.Windows;
using ShoppingCartSimulation.Data;

namespace ShoppingCartSimulation
{
    /// <summary>
    /// Interaction logic for PayWindow.xaml
    /// </summary>
    public partial class PayWindow : Window
    {
        private ShoppingCart cart;
        private string customerName;
        public PayWindow(ShoppingCart c, string n)
        {
            InitializeComponent();
            customerName = n;
            cart = c;
            Total.Content = "$" + cart.Total.ToString("0.00");
            SetPaymentFrame();
        }


        /// <summary>
        /// Update the payment frame (on window init and on radio button pressed)
        /// </summary>
        void SetPaymentFrame()
        {
            PaymentFrame.NavigationUIVisibility = System.Windows.Navigation.NavigationUIVisibility.Hidden;
            PaymentFrame.Content = null;
            if(CashButton.IsChecked == true)
                PaymentFrame.Content = new PayWithCashPage(cart.Total, this);
            if(CreditCardButton.IsChecked == true)
                PaymentFrame.Content = new CreditCardPage(cart.Total, this);
        }

        /// <summary>
        /// When the Radio Button is pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SelectPaymentOption(object sender, EventArgs e)
        {
            SetPaymentFrame();
        }

        /// <summary>
        /// When the pay button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PayButton_Click(object sender, RoutedEventArgs e)
        {
            double total = cart.Total;
            if (CashButton.IsChecked == true)
            {
                PayWithCashPage page = (PayWithCashPage)PaymentFrame.Content;
                total = double.Parse(page.CashAmount.Text);
                cart.Pay(true, total, customerName);
            }
            if (CreditCardButton.IsChecked == true)
            {
                CreditCardPage page = (CreditCardPage) PaymentFrame.Content;
                total += double.Parse(page.CashBackText.Text);
                cart.Pay(false, total, customerName);
            }

            this.Close();

        }
        
    }
}
