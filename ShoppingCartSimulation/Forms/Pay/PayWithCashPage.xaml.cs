﻿using System;
using System.Windows.Controls;
using System.Windows.Media;


namespace ShoppingCartSimulation
{
    /// <summary>
    /// Interaction logic for PayWithCashPage.xaml
    /// </summary>
    public partial class PayWithCashPage : Page
    {
        double total;
        PayWindow parent;
        public PayWithCashPage(double t, PayWindow p)
        {
            InitializeComponent();
            parent = p;
            total = t;

            ChangePayment(null, null);  //Make sure the change shows up ASAP
        }

        /// <summary>
        /// When the change amount text box is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ChangePayment(object sender, EventArgs e)
        {
            if (ChangeAmount == null)  //If the event is called before init
                return;
            double amt = 0.0;
            if(double.TryParse(CashAmount.Text, out amt))
            {
                double change = amt - total;
                if(change < 0)
                {
                    ChangeAmount.Foreground = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                    ChangeAmount.Text = "$" + change.ToString("0.00");
                    parent.PayButton.IsEnabled = false;
                    parent.ErrorText.Text = "[Amount less than total]";
                }
                else
                {
                    ChangeAmount.Foreground = new SolidColorBrush(Color.FromRgb(0, 155, 0));
                    ChangeAmount.Text = "+$" + change.ToString("0.00");
                    parent.PayButton.IsEnabled = true;
                    parent.ErrorText.Text = "";
                }
            }
            else
            {
                CashAmount.Text = "0.00";
            }
        }
    }
}
