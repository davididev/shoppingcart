---------------------------------------
----- Adding items to the database ----
---------------------------------------
There are two places you need to update:
* Right click ShoppingCartSimulation --> Properties --> Resources
  First line in Database.txt is the categories, seperated by ','
  Lines 2+ are the items, seperated by ','

  Arguments in the items:
    1. Item ID: three digit number.  First digit is the category,
	   digits 2-3 are the item ID.  (starting with 01)
	2. Item name, as shown in the receipt.  Keep it up to 10 characters
	3. The price of the item
	4. Whether or not it's a tax.  Type in a 1 if it's taxed, 0 if not.

* App.xaml: in <Application.Resources>, add a bitmap image with
  the key of the Item ID.
* Using the solution explorer, add a bitmap image into folder
  Resources2.


---------------------------------------
------ Using the Shopping Center ------
---------------------------------------
* On the left is the list of items you have purchased.
* To select items, look at the images on the right.
* If you hover over them, you can see the details of the item, 
  including the item ID.  If you type the item ID into the top
  right text box, you can include that one item.
* If you added an item accidentally, click the "Void Item" button
  to take out the last thing you put in.
* If you want to start over, click "Clear Cart."
* When you're ready, click "Check Out"


---------------------------------------
------------- Checking out ------------
---------------------------------------
* If you prefer to pay with credit card, press the Credit Card radio
  button.  Then fill out your credit card information.  
  The expiration date is shown on your card, showing the month and year.
  For instance, is January 2021 is the expiration date, do 01/21
  The security code is located on the back of your card and is 
  three digits.
  If you want cash back, type any amount.
* If you prefer to pay cash, keep the radio button the over cash.
  Be sure to type in an amount greater than the total or you cannot pay.
* Once the pay button is enabled, click the pay button and your receipt
  will open in the browser.